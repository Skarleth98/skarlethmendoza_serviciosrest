package edu.uoc.android.restservice.ui.enter;

import android.app.Application;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Owner;

/**
 * Created by edgardopanchana on 4/29/18.
 */

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {


    // Crear nombres de recursos

    TextView etiNombre;
    TextView etiID;
    ImageView imagen;
    ArrayList<Owner> listaFollowers;

    public AdaptadorFollowers(ArrayList<Owner> listaFollowers,InfoUserActivity infoUserActivity) {
        this.listaFollowers = listaFollowers;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {

        //Asignar valores a los recursos deacuaerdo a la posicion de la lista

        etiNombre.setText("Nombre: "+listaFollowers.get(position).getLogin());
        etiID.setText("ID: "+listaFollowers.get(position).getId().toString ());
// Obtener url de la imagen a mostrar
        String urlimagen = listaFollowers.get(position).getAvatarUrl();
// Mostrar imagen de la libreria picasso
        Picasso.get()
                .load(urlimagen)
                .error(R.mipmap.ic_launcher)
                .fit()
                .centerInside()
                .into(imagen);

    }

    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {



        public ViewHolderFollowers(View itemView) {
            super(itemView);
// llamar a los recursos
            etiNombre = (TextView) itemView.findViewById(R.id.textViewLista);
            etiID = (TextView) itemView.findViewById(R.id.textViewListaid);
            imagen = (ImageView) itemView.findViewById(R.id.imageViewLista);

        }
    }
}
